<?php
/**
 * Copyright © Vnecoms, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\VendorsRouter\Controller\Vendors\Index;

/**
 * Class Index
 * @package Vnecoms\VendorsRouter\Controller\Vendors\Route
 */
class Index extends \Vnecoms\Vendors\Controller\Vendors\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    protected $_aclResource = 'Vnecoms_VendorsRouter::route_index';

    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $title = $this->_view->getPage()->getConfig()->getTitle();
        $this->setActiveMenu('Vnecoms_VendorsRouter::route_index');
        $title->prepend(__("Vendors"));
        $title->prepend(__("Manage Route"));
        $this->_addBreadcrumb(__("Route"), __("Route"))->_addBreadcrumb(__("Manage Route"), __("Manage Route"));
        $this->_view->renderLayout();
    }
}
